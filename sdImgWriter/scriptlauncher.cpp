#include "scriptlauncher.h"
#include <QProcess>
#include <QDebug>
#include <string>
using namespace std;

//Constructor
ScriptLauncher::ScriptLauncher(QObject *parent):
    QObject(parent), m_process(new QProcess(this))
{
}

//Function that starts the process to execute the terminal command and establish connections between signals and slots
void ScriptLauncher::launchScript(const QString &program, const QStringList &arguments){

    //Connect a signal (action) to a slot (reaction)
    //When process error occurs (signal); emit function (slot) to notify user
    connect(m_process, SIGNAL(error(QProcess::ProcessError)), this, SLOT(processError()));
    //When process/script has finished (signal) or bash script error occured (signal); emit function (slot) to notify user
    connect(m_process, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(scriptDoneOrError()));

    qDebug() << "\nExecuting: " << program << arguments << endl;
    //start the process, when called the name of the program and arguments go in as parameters
    m_process->start(program, QStringList() << "-c" << arguments);
}


//Slot to notify user if there are any errors or if the script completed successfully.
//errorQML() called from Connections in main.qml
void ScriptLauncher::scriptDoneOrError(){
    m_process->waitForFinished(-1);
    QString errMsg = m_process->readAllStandardError(); //terminal error
    QString outMsg = m_process->readAllStandardOutput();//terminal output
    //QString channel = m_process->readChannel();//terminal output
    QProcess::ExitStatus exitMsg = m_process->exitStatus();//Status on whether the process exited properly or not
    qDebug() << "Errors: \n" << errMsg << "\nOutput: \n" << outMsg << "\n" << exitMsg <<endl;

    //If statements to evaluate errMsg value to emit the respective function in QML to print message to the user

    //The terminal completion message for dd command classes as an error, so if the error message contains "copied" then
    //dd was successful
    if(errMsg.contains("copied")){
        outMsg=errMsg;
        errMsg="";
    }

    if(errMsg.contains("Killed")) //Sometimes terminal doesn't give "Killed" feedback, if so, app goes to normal error screen
        emit timeoutQML();
    else if(errMsg.contains("")){ //it will show Error screen if testing without dd command
        //it will go to Error screen if testing without dd command, last resort change to errMsg=""
        //also a problem if dd doesn't actually complete and says "0bytes copied"
        //outMsg=errMsg;
        emit scriptDoneQML(outMsg);
    }
    else
        emit errorQML(errMsg);
}

//Slot to notify user if there has been a process error
//errorQML() called from Connections in main.qml
void ScriptLauncher::processError(){
    m_process->waitForFinished(-1);
    QString errMsg = m_process->readAllStandardError(); //terminal error
    QString outMsg = m_process->readAllStandardOutput();//terminal output
    QProcess::ExitStatus exitMsg = m_process->exitStatus();//Status on whether the process exited properly or not
    qDebug() << "Errors: \n" << errMsg << "\nOutput: \n" << outMsg << "\n" << exitMsg << endl;
    emit processErrorQML();
}
