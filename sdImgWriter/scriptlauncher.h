#ifndef SCRIPTLAUNCHER_H
#define SCRIPTLAUNCHER_H

#include <QObject>
#include <QProcess>

class ScriptLauncher : public QObject
{
    Q_OBJECT

public:
    //Constructor
    explicit ScriptLauncher(QObject *parent = 0);

    //launchScript function with two parameters;
    //the program, and the arguments input to the application
    Q_INVOKABLE void launchScript(const QString &program,
                                  const QStringList &arguments);

//Signals - events that occur
signals:
    void scriptDoneQML(QString outMsg);
    void errorQML(QString errMsg);
    void processErrorQML();
    void timeoutQML();


//Slots - functions that react to the signals (events)
public slots:
    void scriptDoneOrError();
    void processError();

private:
    //Declare the Process object as a pointer
    QProcess *m_process;

};
#endif // SCRIPTLAUNCHER_H


