import QtQuick 2.8
import QtQuick.Window 2.2
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import ScriptLauncher 1.0

Window {
    visible: true
    width: 800
    height: 480
    title: qsTr("SD Card Programmer")

    //ScriptLauncher object
    ScriptLauncher {
        id: launcher
    }

    //Connect the c++ signals and qml slots
    Connections{
        target: launcher

        //notifies user when the script is done
        onScriptDoneQML: {
            color = "#83ff64" //green
            busyIndicator.visible = false
            msgOutput.clear()
            msgOutput.color = "#0066b3"
            msgOutput.append("Image successfully written to SD Card."
                             + "\n You may safely swap/remove the SD card to write to another." /*+ "\nOutput: " + outMsg*/)
        }

        //notifies user whent there is an error in the process of writing the image
        onErrorQML: {
            color = "red"
            busyIndicator.visible = false
            msgOutput.clear()
            msgOutput.color = "white"
            msgOutput.append("Error: " + errMsg)
        }

        //notifies user when process error
        onProcessErrorQML: {
            color = "red"
            busyIndicator.visible = false
            msgOutput.clear()
            msgOutput.color = "white"
            msgOutput.append("Process Error - please get software engineer to ensure the correct bash script is being executed.")
        }

        //notifies user when the writing image process has timed out, likely due to removed SD card
        onTimeoutQML: {
            color = "red"
            busyIndicator.visible = false
            msgOutput.clear()
            msgOutput.color = "white"
            msgOutput.append("Process Timed-out - The SD card was probably removed before completion. Please reinsert SD card and try again.")
        }
    }

    //Application heading title
    Text {
        id: subtitle
        anchors{
            horizontalCenter: parent.horizontalCenter
            top: parent.top
            topMargin: 20
        }
        color: "#0066b3"
        text: qsTr("SD Card Programmer")
        font.bold: true
        font.italic: true
        font.family: "Arial"
        font.pixelSize: 20
    }

    //Itron logo
    Image {
        id: image
        anchors.horizontalCenterOffset: -175
        anchors.centerIn: parent
        width: 194
        height: 85
        fillMode: Image.PreserveAspectFit
        source: "itron logo.png"
    }

    //ScrollView{  //need Qt 5.9
    //    id: scroll

        //Messages to the user
        TextArea {
            id: msgOutput
            width: 544
            height: 114
            anchors {
                horizontalCenter: parent.horizontalCenter
                top: subtitle.bottom
            }
            color: "#0066b3"            
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            font.family: "Courier"
            font.pixelSize: 16
            font.bold: true
        }
    //}

    //Animated symbol that shows the user that the process is still in progress
    BusyIndicator {
        id: busyIndicator
        clip: false
        anchors.centerIn: parent
        anchors.horizontalCenterOffset: -175
        anchors.verticalCenterOffset: 75
        visible: false
    }

    //Button area
    ColumnLayout {
        id: columnLayout1
        anchors.horizontalCenterOffset: 125
        anchors.centerIn: parent
        spacing: parent.height / 15

        //Button that executes the bash script to write the Image to the SD card and updates the MAC addresses
        Button {
            id: remsdaqButton
            text: qsTr("Write Remsdaq Image to SD Card")
            Layout.fillWidth: true

            background: Rectangle {
                id: remRec
                color: "#0066b3"
                radius: 20 //rounded corners
            }
            contentItem: Text {
                color: "white"
                text: remsdaqButton.text
                horizontalAlignment: Text.AlignHCenter
            }
            onClicked:{
                color = "white"
                msgOutput.clear()
                msgOutput.color = "#0066b3"
                msgOutput.append("Writing Image. This may take a few minutes...")
                busyIndicator.visible = true

                //Execute the bash script to write the image to the SD card
                launcher.launchScript("./writeRemsdaq.sh", "")
            }
            onPressed: {
                scale = 0.9
                remRec.color = "#2185d0"
            }
            onReleased: {
                scale = 1.0
                remRec.color = "#0066b3"
            }
        }

        //Button the exits the application
        Button {
            id: quit
            text: qsTr("Quit")
            Layout.fillWidth: true
            background: Rectangle {
                id: quitRec
                color: "#0066b3"
                radius: 20 //rounded corners
            }
            contentItem: Text {
                color: "white"
                text: quit.text
                horizontalAlignment: Text.AlignHCenter
            }
            onClicked: {
                console.log("Quitting Application")
                Qt.quit();
            }
            onPressed: {
                scale = 0.9
                quitRec.color = "#2185d0"
            }
            onReleased: {
                scale = 1.0
                quitRec.color = "#0066b3"
            }
        }

        /*
        //User Guide opens with help for the user on how to use/ FAQ
        Button {
            id: help
            text: qsTr("Help?")
            Layout.fillWidth: true
            background: Rectangle {
                color: "#0066b3"
            }
            contentItem: Text {
                color: "white"
                text: help.text
                horizontalAlignment: Text.AlignHCenter
            }
            onClicked: {
                msgOutput.clear()
                msgOutput.append("User Guide available soon.")
                //open window with user guidance

            }
        }
        */
    }
}
