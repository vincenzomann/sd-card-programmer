#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QProcess>
#include <QQuickWindow>
#include <QQmlContext>
#include "scriptlauncher.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    //Allows main.qml to use the c++ class ScriptLauncher
    qmlRegisterType<ScriptLauncher>("ScriptLauncher", 1, 0, "ScriptLauncher");

    QQmlApplicationEngine engine;

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
