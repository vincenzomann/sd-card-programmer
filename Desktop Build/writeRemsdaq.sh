#!/bin/bash
#DESKTOP


#Script to program sd cards with the Remsdaq image and update MAC addresses

#Write image to sd card. Change to sdb to sda when put app to run on boot
#umount /dev/sdb* #|| true

#set -e #kills bash script if there's an error with the process. However
        #it classes umount output as an error, need an alternative

#write image to sd card. Kill process after 10 min timeout, prevents
#frozen process if the sd card is removed part way
#timeout -s KILL 10m dd if=/home/root/Remsdaq/SW00083_v2_0_0.img of=/dev/sdb bs=4M
#sync
#umount /dev/sdb*

#Update MAC address

#mount /dev/sdb3 /media/clone #mount sd card


#MAC addresses from default script
defaultMAC1="00:0E:BF:03:03:BE"
defaultMAC2="00:0E:BF:03:03:BF"

#Previous MAC address saved from file
if [ ! -f ~/Documents/MACtest/latestMAC.txt ] ; then
  prevMAC="AB:CD:EF:12:34:56"
  else  # otherwise read the value from the file
    prevMAC=$(< ~/Documents/MACtest/latestMAC.txt)
fi

mac1hex=$( echo "$prevMAC" | tr -d ':' ) #removes the colons
mac2hex=$( echo "$prevMAC" | tr -d ':' ) #removes the colons

mac1dec=$( echo $((0x$mac1hex))) #convert hex to dec
mac2dec=$( echo $((0x$mac2hex))) #convert hex to dec

newMAC1=$(($mac1dec + 1))  #increment by 1 to previous mac address
newMAC2=$(($newMAC1 + 1))  #increment by 1 to previous mac address

newMAC1=$( printf "%012x\n" $newMAC1) #convert back to hex and pad with 0s
newMAC2=$( printf "%012x\n" $newMAC2) #convert back to hex and pad with 0s

newMAC1=$(echo $newMAC1 | awk '{print toupper($0)}') #Convert to upper case
newMAC2=$(echo $newMAC2 | awk '{print toupper($0)}') #Convert to upper case

newMAC1=$(echo $newMAC1 | sed 's|..|&:|g;s|:$||') #inserts colons and removes last trailing colon
newMAC2=$(echo $newMAC2 | sed 's|..|&:|g;s|:$||') #inserts colons and removes last trailing colon

#replaces addresses into different file, remove last path to replace into same file and use flag -i
sed "s|$defaultMAC1|$newMAC1|g;s|$defaultMAC2|$newMAC2|g" ~/Documents/MACtest/etc/rc.d/init.d/itron.sh > ~/Documents/MACtest/new.txt

#append new MAC addresses to txt file

# Previous Counter saved from file. USed to number the log of MAC addresses for production
# if we don't have a file, start at zero
if [ ! -f ~/Documents/MACtest/counter.txt ] ; then
  countMAC=0
  else  # otherwise read the value from the file
    countMAC=$(< ~/Documents/MACtest/counter.txt)
fi

#Error Handling to not update MAC address and counter if process is killed
#
# TO DO
#

printf "\n$((++countMAC))\t$newMAC1\n$((++countMAC))\t$newMAC2" >> ~/Documents/MACtest/macList.txt

echo ${countMAC} > ~/Documents/MACtest/counter.txt #permanently keep latest counter number

echo ${newMAC2} > ~/Documents/MACtest/latestMAC.txt #permanently keep latest MAC Address


#Copy pointercal file to SD card
#cp /home/root/Remsdaq/pointercal /media/clone/etc


#unmount sd card
#umount /dev/sdb3

#closes script process
exit 0
