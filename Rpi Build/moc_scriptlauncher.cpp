/****************************************************************************
** Meta object code from reading C++ file 'scriptlauncher.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../sdImgWriter/scriptlauncher.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'scriptlauncher.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ScriptLauncher_t {
    QByteArrayData data[13];
    char stringdata0[142];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ScriptLauncher_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ScriptLauncher_t qt_meta_stringdata_ScriptLauncher = {
    {
QT_MOC_LITERAL(0, 0, 14), // "ScriptLauncher"
QT_MOC_LITERAL(1, 15, 13), // "scriptDoneQML"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 6), // "outMsg"
QT_MOC_LITERAL(4, 37, 8), // "errorQML"
QT_MOC_LITERAL(5, 46, 6), // "errMsg"
QT_MOC_LITERAL(6, 53, 15), // "processErrorQML"
QT_MOC_LITERAL(7, 69, 10), // "timeoutQML"
QT_MOC_LITERAL(8, 80, 17), // "scriptDoneOrError"
QT_MOC_LITERAL(9, 98, 12), // "processError"
QT_MOC_LITERAL(10, 111, 12), // "launchScript"
QT_MOC_LITERAL(11, 124, 7), // "program"
QT_MOC_LITERAL(12, 132, 9) // "arguments"

    },
    "ScriptLauncher\0scriptDoneQML\0\0outMsg\0"
    "errorQML\0errMsg\0processErrorQML\0"
    "timeoutQML\0scriptDoneOrError\0processError\0"
    "launchScript\0program\0arguments"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ScriptLauncher[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   49,    2, 0x06 /* Public */,
       4,    1,   52,    2, 0x06 /* Public */,
       6,    0,   55,    2, 0x06 /* Public */,
       7,    0,   56,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    0,   57,    2, 0x0a /* Public */,
       9,    0,   58,    2, 0x0a /* Public */,

 // methods: name, argc, parameters, tag, flags
      10,    2,   59,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::QStringList,   11,   12,

       0        // eod
};

void ScriptLauncher::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ScriptLauncher *_t = static_cast<ScriptLauncher *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->scriptDoneQML((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->errorQML((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->processErrorQML(); break;
        case 3: _t->timeoutQML(); break;
        case 4: _t->scriptDoneOrError(); break;
        case 5: _t->processError(); break;
        case 6: _t->launchScript((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QStringList(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (ScriptLauncher::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ScriptLauncher::scriptDoneQML)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (ScriptLauncher::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ScriptLauncher::errorQML)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (ScriptLauncher::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ScriptLauncher::processErrorQML)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (ScriptLauncher::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ScriptLauncher::timeoutQML)) {
                *result = 3;
                return;
            }
        }
    }
}

const QMetaObject ScriptLauncher::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ScriptLauncher.data,
      qt_meta_data_ScriptLauncher,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ScriptLauncher::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ScriptLauncher::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ScriptLauncher.stringdata0))
        return static_cast<void*>(const_cast< ScriptLauncher*>(this));
    return QObject::qt_metacast(_clname);
}

int ScriptLauncher::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void ScriptLauncher::scriptDoneQML(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ScriptLauncher::errorQML(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void ScriptLauncher::processErrorQML()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void ScriptLauncher::timeoutQML()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
